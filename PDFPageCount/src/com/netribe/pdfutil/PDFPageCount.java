package com.netribe.pdfutil;

import java.io.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.io.*;


public class PDFPageCount {
	
	PDFPageCount(){  
		
     }  

	
   public static int efficientPDFPageCount(String nomeFile) {
	     
		 int pages = 0;
		 
		 try {
			 File file = new File(nomeFile);
				
			if(file.exists()) {
			 	 
				 RandomAccessFile raf = new RandomAccessFile(file, "r");
				 RandomAccessFileOrArray pdfFile = new RandomAccessFileOrArray(
					  new RandomAccessSourceFactory().createSource(raf));
				 PdfReader reader = new PdfReader(pdfFile, new byte[0]);
				 pages = reader.getNumberOfPages();
				 reader.close();
				
			} else System.out.println("File non trovato!");
			
			 return pages;
			 
		 } catch(Exception ex) {
		      return pages;	 
		 }
	  }

}
