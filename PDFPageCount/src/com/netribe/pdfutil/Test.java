package com.netribe.pdfutil;

import java.io.File;

public class Test {

public static void main(String[] args) {
		
		int count;
		try {
			String nomeFile = args[0];
			nomeFile = nomeFile == null ? "" : nomeFile.trim();
			
			count = 0;
			
			if(nomeFile.length() ==0) System.out.println("File non passato");
			else {
			
					count = PDFPageCount.efficientPDFPageCount(nomeFile);
					System.out.println("Numero pagine " +count);
				
				
			}
			
			
		} catch (Exception e) {
			System.out.println("Errore durente l'esecuzione " + e.toString());
		}
		
		

	}
	

}
